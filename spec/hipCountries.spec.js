'use strict';

describe("getCodes", function () {
    it("should return 249 country codes in the English language", function () {
        hipCountries.getCodes('en').should.have.length(249);
    });
    it("should return the 249 country codes without supplying a parameter", function () {
        hipCountries.getCodes().should.have.length(249);
    });
});
describe("getNames", function () {
    it("should return 249 country names in the English language", function () {
        hipCountries.getNames('en').should.have.length(249);
    });
    it("should return the 249 country names without supplying a parameter", function () {
        hipCountries.getNames().should.have.length(249);
    });
    it("should return 249 country names in the English language when the country names are not known in the given language", function () {
        hipCountries.getNames('xx').should.have.length(249);
    });
});
describe("getNameByCode", function () {
    it("should return 'United Kingdom' for 'GB'", function () {
        hipCountries.getNameByCode('GB').should.equal('United Kingdom');
    });
    it("should return 'Netherlands' for 'NL'", function () {
        hipCountries.getNameByCode('NL').should.equal('Netherlands');
    });
    it("should return 'unknown' for 'xx'", function () {
        hipCountries.getNameByCode('xx').should.equal('unknown');
    });
    it("should return 'unknown' when no code is specified", function () {
        hipCountries.getNameByCode().should.equal('unknown');
    });
});
describe("getNameByCode with a language parameter", function () {
    it("should return 'Macedoine, l'ex-Republique Yougoslave de' in 'fr' for 'MK'", function () {
        hipCountries.getNameByCode('MK', 'fr').should.equal('Macedoine, l\'ex-Republique Yougoslave de');
    });
    it("should return 'Reino Unido (el)' in 'es' for 'GB'", function () {
        hipCountries.getNameByCode('GB', 'es').should.equal('Reino Unido (el)');
    });
    it("should return 'Bangladesch' in 'de' for 'BD'", function () {
        hipCountries.getNameByCode('BD', 'de').should.equal('Bangladesch');
    });
    it("should return 'unknown' in 'es' for 'xx'", function () {
        hipCountries.getNameByCode('xx', 'es').should.equal('unknown');
    });
    it("should return 'unknown' in 'es' when no code is specified", function () {
        hipCountries.getNameByCode(null, 'es').should.equal('unknown');
    });
});
