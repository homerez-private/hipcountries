'use strict';
describe('orderByCountryName', function() {
  beforeEach(module('homerez.countries'));

  describe('orderByCountryName in English', function() {
    beforeEach(inject(function(_$translate_) {
      sinon.stub(_$translate_, 'use', function() {return 'en'});
    }));

    it('should return as many results as hipCountries.getCodes does',
      inject(function(orderByCountryNameFilter) {
        var result = orderByCountryNameFilter(hipCountries.getCodes());
        expect(result).to.have.length(hipCountries.getCodes().length);
    }));

    it('should show "Algeria" before "American Samoa"',
      inject(function(orderByCountryNameFilter) {
        var result = orderByCountryNameFilter(hipCountries.getCodes());
        expect(result.indexOf('DZ')).to.be.below(result.indexOf('AS'))
    }));

  })

})
describe('countryName', function() {

  beforeEach(module('homerez.countries'));

  var tests = [
    {
      blockName: 'countryNames in English',
      language: 'en',
      testCases: [
        {
          countryCode: 'NL', 
          expectedCountryName: 'Netherlands'
        },
        {
          countryCode: 'GB', 
          expectedCountryName: 'United Kingdom'
        }
      ]
    },
    {
      blockName: 'countryNames in Spanish', 
      language: 'es',
      testCases: [
        {
          countryCode: 'AE',
          expectedCountryName: 'Emiratos Árabes Unidos (Los)'
        },
        {
          countryCode: 'BT',
          expectedCountryName: 'Bután'
        }
      ]
    },
    {
      blockName: 'countryNames in French', 
      language: 'fr',
      testCases: [
        {
          countryCode: 'GB',
          expectedCountryName: 'Royaume-uni'
        },
        {
          countryCode: 'IO',
          expectedCountryName: 'Ocean Indien, Territoire Britannique de l\''
        }
      ]
    },
    {
      blockName: 'countryNames in German', 
      language: 'de',
      testCases: [
        {
          countryCode: 'MP',
          expectedCountryName: 'Nördliche Marianen'
        },
        {
          countryCode: 'TF',
          expectedCountryName: 'Französische Süd- und Antarktisgebiete'
        }
      ]
    }
  ];
  for (var i = 0; i < tests.length; i++) {
    var t = tests[i];
    describe(t.blockName, function() {

      var lang = t.language;
      beforeEach(inject(function(_$translate_) {
        sinon.stub(_$translate_, 'use', function() {return lang});
      }));

      for (var j = 0; j<t.testCases.length; j++) {
        var tc = t.testCases[j];
        var testName = 'should convert "'+tc.countryCode+'" into "'+tc.expectedCountryName+'"';

        it(testName, inject(function(countryNameFilter) {
          expect(countryNameFilter(tc.countryCode)).to.equal(tc.expectedCountryName);
        }));
      }
    });
  }  
});
