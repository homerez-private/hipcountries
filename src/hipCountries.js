(function (root, factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define([], factory);
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory();
  } else {
    // Browser globals (root is window)
    root.hipCountries = factory();
  }
}(this, function () {

  var countryData = {
    AD: {
      fr: 'Andorre',
      es: 'Andorra',
      en: 'Andorra',
      de: 'Andorra'
    },
    AE: {
      fr: 'Emirats Arabes Unis',
      es: 'Emiratos Árabes Unidos (Los)',
      en: 'United Arab Emirates',
      de: 'Vereinigte Arabische Emirate'
    },
    AF: {
      fr: 'Afghanistan',
      es: 'Afganistán',
      en: 'Afghanistan',
      de: 'Afghanistan'
    },
    AG: {
      fr: 'Antigua et Barbuda',
      es: 'Antigua y Barbuda',
      en: 'Antigua and Barbuda',
      de: 'Antigua und Barbuda'
    },
    AI: {
      fr: 'Anguilla',
      es: 'Anguila',
      en: 'Anguilla',
      de: 'Anguilla'
    },
    AL: {
      fr: 'Albanie',
      es: 'Albania',
      en: 'Albania',
      de: 'Albanien'
    },
    AM: {
      fr: 'Armenie',
      es: 'Armenia',
      en: 'Armenia',
      de: 'Armenien'
    },
    AO: {
      fr: 'Angola',
      es: 'Angola',
      en: 'Angola',
      de: 'Angola'
    },
    AQ: {
      fr: 'Antarctique',
      es: 'Antártida',
      en: 'Antarctica',
      de: 'Antarktika'
    },
    AR: {
      fr: 'Argentine',
      es: 'Argentina',
      en: 'Argentina',
      de: 'Argentinien'
    },
    AS: {
      fr: 'Samoa Americaines',
      es: 'Samoa Americana',
      en: 'American Samoa',
      de: 'Amerikanisch-Samoa'
    },
    AT: {
      fr: 'Autriche',
      es: 'Austria',
      en: 'Austria',
      de: 'Österreich'
    },
    AU: {
      fr: 'Australie',
      es: 'Australia',
      en: 'Australia',
      de: 'Australien'
    },
    AW: {
      fr: 'Aruba',
      es: 'Aruba',
      en: 'Aruba',
      de: 'Aruba'
    },
    AX: {
      fr: 'Åland, Îles',
      es: 'Islas Åland',
      en: 'Åland Islands',
      de: 'Åland'
    },
    AZ: {
      fr: 'Azerbaidjan',
      es: 'Azerbaiyán',
      en: 'Azerbaijan',
      de: 'Aserbaidschan'
    },
    BA: {
      fr: 'Bosnie-Herzegovine',
      es: 'Bosnia y Herzegovina',
      en: 'Bosnia and Herzegovina',
      de: 'Bosnien und Herzegowina'
    },
    BB: {
      fr: 'Barbade',
      es: 'Barbados',
      en: 'Barbados',
      de: 'Barbados'
    },
    BD: {
      fr: 'Bangladesh',
      es: 'Bangladés',
      en: 'Bangladesh',
      de: 'Bangladesch'
    },
    BE: {
      fr: 'Belgique',
      es: 'Bélgica',
      en: 'Belgium',
      de: 'Belgien'
    },
    BF: {
      fr: 'Burkina Faso',
      es: 'Burkina Faso',
      en: 'Burkina Faso',
      de: 'Burkina Faso'
    },
    BG: {
      fr: 'Bulgarie',
      es: 'Bulgaria',
      en: 'Bulgaria',
      de: 'Bulgarien'
    },
    BH: {
      fr: 'Bahrein',
      es: 'Baréin',
      en: 'Bahrain',
      de: 'Bahrain'
    },
    BI: {
      fr: 'Burundi',
      es: 'Burundi',
      en: 'Burundi',
      de: 'Burundi'
    },
    BJ: {
      fr: 'Benin',
      es: 'Benín',
      en: 'Benin',
      de: 'Benin'
    },
    BL: {
      fr: 'Saint-Barthelemy',
      es: 'San Bartolomé',
      en: 'Saint Barthélemy',
      de: 'Saint-Barthélemy'
    },
    BM: {
      fr: 'Bermudes',
      es: 'Bermudas',
      en: 'Bermuda',
      de: 'Bermuda'
    },
    BN: {
      fr: 'Brunei Darussalam',
      es: 'Brunéi Darussalam',
      en: 'Brunei Darussalam',
      de: 'Brunei Darussalam'
    },
    BO: {
      fr: 'Bolivie, l\'Etat Plurinational de',
      es: 'Bolivia, Estado Plurinacional de',
      en: 'Bolivia, Plurinational State of',
      de: 'Bolivien'
    },
    BQ: {
      fr: 'Bonaire, Saint-Eustache et Saba',
      es: 'Bonaire, San Eustaquio y Saba',
      en: 'Bonaire, Sint Eustatius and Saba',
      de: 'Bonaire, Sint Eustatius und Saba (Niederlande)'
    },
    BR: {
      fr: 'Bresil',
      es: 'Brasil',
      en: 'Brazil',
      de: 'Brasilien'
    },
    BS: {
      fr: 'Bahamas',
      es: 'Bahamas (las)',
      en: 'Bahamas',
      de: 'Bahamas'
    },
    BT: {
      fr: 'Bhoutan',
      es: 'Bután',
      en: 'Bhutan',
      de: 'Bhutan'
    },
    BV: {
      fr: 'Bouvet, Ile',
      es: 'Isla Bouvet',
      en: 'Bouvet Island',
      de: 'Bouvetinsel'
    },
    BW: {
      fr: 'Botswana',
      es: 'Botsuana',
      en: 'Botswana',
      de: 'Botswana'
    },
    BY: {
      fr: 'Belarus',
      es: 'Bielorrusia',
      en: 'Belarus',
      de: 'Belarus (Weißrussland)'
    },
    BZ: {
      fr: 'Belize',
      es: 'Belice',
      en: 'Belize',
      de: 'Belize'
    },
    CA: {
      fr: 'Canada',
      es: 'Canadá',
      en: 'Canada',
      de: 'Kanada'
    },
    CC: {
      fr: 'Cocos (Keeling), Iles',
      es: 'Islas Cocos (Keeling)',
      en: 'Cocos (Keeling) Islands',
      de: 'Kokosinseln'
    },
    CD: {
      fr: 'Congo, La Republique Democratique du',
      es: 'Congo (la República Democrática del)',
      en: 'Congo, the Democratic Republic of the',
      de: 'Kongo, Demokratische Republik (ehem. Zaire)'
    },
    CF: {
      fr: 'Centrafricaine, Republique',
      es: 'República Centroafricana (la)',
      en: 'Central African Republic',
      de: 'Zentralafrikanische Republik'
    },
    CG: {
      fr: 'Congo',
      es: 'Congo',
      en: 'Congo',
      de: 'Republik Kongo'
    },
    CH: {
      fr: 'Suisse',
      es: 'Suiza',
      en: 'Switzerland',
      de: 'Schweiz (Confoederatio Helvetica)'
    },
    CI: {
      fr: 'Cote d\'Ivoire',
      es: 'Côte d\'Ivoire',
      en: 'Côte d\'Ivoire',
      de: 'Côte d’Ivoire (Elfenbeinküste)'
    },
    CK: {
      fr: 'Cook, Iles',
      es: 'Islas Cook (las)',
      en: 'Cook Islands',
      de: 'Cookinseln'
    },
    CL: {
      fr: 'Chili',
      es: 'Chile',
      en: 'Chile',
      de: 'Chile'
    },
    CM: {
      fr: 'Cameroun',
      es: 'Camerún',
      en: 'Cameroon',
      de: 'Kamerun'
    },
    CN: {
      fr: 'Chine',
      es: 'China',
      en: 'China',
      de: 'China, Volksrepublik'
    },
    CO: {
      fr: 'Colombie',
      es: 'Colombia',
      en: 'Colombia',
      de: 'Kolumbien'
    },
    CR: {
      fr: 'Costa Rica',
      es: 'Costa Rica',
      en: 'Costa Rica',
      de: 'Costa Rica'
    },
    CU: {
      fr: 'Cuba',
      es: 'Cuba',
      en: 'Cuba',
      de: 'Kuba'
    },
    CV: {
      fr: 'Cap-vert',
      es: 'Cabo Verde',
      en: 'Cape Verde',
      de: 'Kap Verde'
    },
    CW: {
      fr: 'Curacao',
      es: 'Curaçao',
      en: 'Curaçao',
      de: 'Curaçao'
    },
    CX: {
      fr: 'Christmas, Ile',
      es: 'Isla de Navidad',
      en: 'Christmas Island',
      de: 'Weihnachtsinsel'
    },
    CY: {
      fr: 'Chypre',
      es: 'Chipre',
      en: 'Cyprus',
      de: 'Zypern'
    },
    CZ: {
      fr: 'Tcheque, Republique',
      es: 'República Checa (la)',
      en: 'Czech Republic',
      de: 'Tschechische Republik'
    },
    DE: {
      fr: 'Allemagne',
      es: 'Alemania',
      en: 'Germany',
      de: 'Deutschland'
    },
    DJ: {
      fr: 'Djibouti',
      es: 'Yibuti',
      en: 'Djibouti',
      de: 'Dschibuti'
    },
    DK: {
      fr: 'Danemark',
      es: 'Dinamarca',
      en: 'Denmark',
      de: 'Dänemark'
    },
    DM: {
      fr: 'Dominique',
      es: 'Dominica',
      en: 'Dominica',
      de: 'Dominica'
    },
    DO: {
      fr: 'Dominicaine, Republique',
      es: 'República Dominicana (la)',
      en: 'Dominican Republic',
      de: 'Dominikanische Republik'
    },
    DZ: {
      fr: 'Algerie',
      es: 'Argelia',
      en: 'Algeria',
      de: 'Algerien'
    },
    EC: {
      fr: 'Equateur',
      es: 'Ecuador',
      en: 'Ecuador',
      de: 'Ecuador'
    },
    EE: {
      fr: 'Estonie',
      es: 'Estonia',
      en: 'Estonia',
      de: 'Estland'
    },
    EG: {
      fr: 'Egypte',
      es: 'Egipto',
      en: 'Egypt',
      de: 'Ägypten'
    },
    EH: {
      fr: 'Sahara Occidental',
      es: 'Sahara Occidental',
      en: 'Western Sahara',
      de: 'Westsahara'
    },
    ER: {
      fr: 'Erythree',
      es: 'Eritrea',
      en: 'Eritrea',
      de: 'Eritrea'
    },

    ES: {
      fr: 'Espagne',
      es: 'España',
      en: 'Spain',
      de: 'Spanien'
    },
    ET: {
      fr: 'Ethiopie',
      es: 'Etiopía',
      en: 'Ethiopia',
      de: 'Äthiopien'
    },
    FI: {
      fr: 'Finlande',
      es: 'Finlandia',
      en: 'Finland',
      de: 'Finnland'
    },
    FJ: {
      fr: 'Fidji',
      es: 'Fiyi',
      en: 'Fiji',
      de: 'Fidschi'
    },
    FK: {
      fr: 'Falkland, Iles (Malvinas)',
      es: 'Islas Malvinas [Falkland] (las)',
      en: 'Falkland Islands (Malvinas)',
      de: 'Falklandinseln'
    },
    FM: {
      fr: 'Micronesie, Etats Federes de',
      es: 'Micronesia (los Estados Federados de)',
      en: 'Micronesia, Federated States of',
      de: 'Mikronesien'
    },
    FO: {
      fr: 'Feroe, Iles',
      es: 'Islas Feroe (las)',
      en: 'Faroe Islands',
      de: 'Färöer'
    },
    FR: {
      fr: 'France',
      es: 'Francia',
      en: 'France',
      de: 'Frankreich'
    },
    GA: {
      fr: 'Gabon',
      es: 'Gabón',
      en: 'Gabon',
      de: 'Gabun'
    },
    GB: {
      fr: 'Royaume-uni',
      es: 'Reino Unido (el)',
      en: 'United Kingdom',
      de: 'Vereinigtes Königreich Großbritannien und Nordirland'
    },
    GD: {
      fr: 'Grenade',
      es: 'Granada',
      en: 'Grenada',
      de: 'Grenada'
    },
    GE: {
      fr: 'Georgie',
      es: 'Georgia',
      en: 'Georgia',
      de: 'Georgien'
    },
    GF: {
      fr: 'Guyane Francaise',
      es: 'Guayana Francesa',
      en: 'French Guiana',
      de: 'Französisch-Guayana'
    },
    GG: {
      fr: 'Guernesey',
      es: 'Guernsey',
      en: 'Guernsey',
      de: 'Guernsey (Kanalinsel)'
    },
    GH: {
      fr: 'Ghana',
      es: 'Ghana',
      en: 'Ghana',
      de: 'Ghana'
    },
    GI: {
      fr: 'Gibraltar',
      es: 'Gibraltar',
      en: 'Gibraltar',
      de: 'Gibraltar'
    },
    GL: {
      fr: 'Groenland',
      es: 'Groenlandia',
      en: 'Greenland',
      de: 'Grönland'
    },
    GM: {
      fr: 'Gambie',
      es: 'Gambia (La)',
      en: 'Gambia',
      de: 'Gambia'
    },
    GN: {
      fr: 'Guinee',
      es: 'Guinea',
      en: 'Guinea',
      de: 'Guinea'
    },
    GP: {
      fr: 'Guadeloupe',
      es: 'Guadalupe',
      en: 'Guadeloupe',
      de: 'Guadeloupe'
    },
    GQ: {
      fr: 'Guinee Equatoriale',
      es: 'Guinea Ecuatorial',
      en: 'Equatorial Guinea',
      de: 'Äquatorialguinea'
    },
    GR: {
      fr: 'Grece',
      es: 'Grecia',
      en: 'Greece',
      de: 'Griechenland'
    },
    GS: {
      fr: 'Georgie du Sud et les Iles Sandwich du Sud',
      es: 'Georgia del sur y las islas sandwich del sur',
      en: 'South Georgia and the South Sandwich Islands',
      de: 'Südgeorgien und die Südlichen Sandwichinseln'
    },
    GT: {
      fr: 'Guatemala',
      es: 'Guatemala',
      en: 'Guatemala',
      de: 'Guatemala'
    },
    GU: {
      fr: 'Guam',
      es: 'Guam',
      en: 'Guam',
      de: 'Guam'
    },
    GW: {
      fr: 'Guinee-Bissau',
      es: 'Guinea-Bisáu',
      en: 'Guinea-Bissau',
      de: 'Guinea-Bissau'
    },
    GY: {
      fr: 'Guyana',
      es: 'Guyana',
      en: 'Guyana',
      de: 'Guyana'
    },
    HK: {
      fr: 'Hong Kong',
      es: 'Hong Kong',
      en: 'Hong Kong',
      de: 'Hongkong'
    },
    HM: {
      fr: 'Heard, Ile Et mcDonald, Iles',
      es: 'Isla Heard e Islas McDonald',
      en: 'Heard Island and McDonald Islands',
      de: 'Heard und McDonaldinseln'
    },
    HN: {
      fr: 'Honduras',
      es: 'Honduras',
      en: 'Honduras',
      de: 'Honduras'
    },
    HR: {
      fr: 'Croatie',
      es: 'Croacia',
      en: 'Croatia',
      de: 'Kroatien'
    },
    HT: {
      fr: 'Haiti',
      es: 'Haití',
      en: 'Haiti',
      de: 'Haiti'
    },
    HU: {
      fr: 'Hongrie',
      es: 'Hungría',
      en: 'Hungary',
      de: 'Ungarn'
    },
    ID: {
      fr: 'Indonesie',
      es: 'Indonesia',
      en: 'Indonesia',
      de: 'Indonesien'
    },
    IE: {
      fr: 'Irlande',
      es: 'Irlanda',
      en: 'Ireland',
      de: 'Irland'
    },
    IL: {
      fr: 'Israel',
      es: 'Israel',
      en: 'Israel',
      de: 'Israel'
    },
    IM: {
      fr: 'Ile De Man',
      es: 'Isla de Man',
      en: 'Isle of Man',
      de: 'Insel Man'
    },
    IN: {
      fr: 'Inde',
      es: 'India',
      en: 'India',
      de: 'Indien'
    },
    IO: {
      fr: 'Ocean Indien, Territoire Britannique de l\'',
      es: 'Territorio Británico del Océano Índico (el)',
      en: 'British Indian Ocean Territory',
      de: 'Britisches Territorium im Indischen Ozean'
    },
    IQ: {
      fr: 'Iraq',
      es: 'Irak',
      en: 'Iraq',
      de: 'Irak'
    },
    IR: {
      fr: 'Iran, Republique Islamique d\'',
      es: 'Irán (la República Islámica de)',
      en: 'Iran, Islamic Republic of',
      de: 'Iran, Islamische Republik'
    },
    IS: {
      fr: 'Islande',
      es: 'Islandia',
      en: 'Iceland',
      de: 'Island'
    },
    IT: {
      fr: 'Italie',
      es: 'Italia',
      en: 'Italy',
      de: 'Italien'
    },
    JE: {
      fr: 'Jersey',
      es: 'Jersey',
      en: 'Jersey',
      de: 'Jersey (Kanalinsel)'
    },
    JM: {
      fr: 'Jamaique',
      es: 'Jamaica',
      en: 'Jamaica',
      de: 'Jamaika'
    },
    JO: {
      fr: 'Jordanie',
      es: 'Jordania',
      en: 'Jordan',
      de: 'Jordanien'
    },
    JP: {
      fr: 'Japon',
      es: 'Japón',
      en: 'Japan',
      de: 'Japan'
    },
    KE: {
      fr: 'Kenya',
      es: 'Kenia',
      en: 'Kenya',
      de: 'Kenia'
    },
    KG: {
      fr: 'Kirghizistan',
      es: 'Kirguistán',
      en: 'Kyrgyzstan',
      de: 'Kirgisistan'
    },
    KH: {
      fr: 'Cambodge',
      es: 'Camboya',
      en: 'Cambodia',
      de: 'Kambodscha'
    },
    KI: {
      fr: 'Kiribati',
      es: 'Kiribati',
      en: 'Kiribati',
      de: 'Kiribati'
    },
    KM: {
      fr: 'Comores',
      es: 'Comoras',
      en: 'Comoros',
      de: 'Komoren'
    },
    KN: {
      fr: 'Saint-Kitts-et-Nevis',
      es: 'San Cristóbal y Nieves',
      en: 'Saint Kitts and Nevis',
      de: 'St. Kitts und Nevis'
    },
    KP: {
      fr: 'Coree, Republique Populaire Democratique de',
      es: 'Corea (la República Democrática Popular de)',
      en: 'Korea, Democratic People\'s Republic of',
      de: 'Korea, Demokratische Volksrepublik (Nordkorea)'
    },
    KR: {
      fr: 'Coree, Republique de',
      es: 'Corea (la República de)',
      en: 'Korea, Republic of',
      de: 'Korea, Republik (Südkorea)'
    },
    KW: {
      fr: 'Koweit',
      es: 'Kuwait',
      en: 'Kuwait',
      de: 'Kuwait'
    },
    KY: {
      fr: 'Caimanes, Iles',
      es: 'Islas Caimán (las)',
      en: 'Cayman Islands',
      de: 'Kaimaninseln'
    },
    KZ: {
      fr: 'Kazakhstan',
      es: 'Kazajistán',
      en: 'Kazakhstan',
      de: 'Kasachstan'
    },
    LA: {
      fr: 'Lao, Republique Democratique Populaire',
      es: 'Lao, (la) República Democrática Popular',
      en: 'Lao People\'s Democratic Republic',
      de: 'Laos, Demokratische Volksrepublik'
    },
    LB: {
      fr: 'Liban',
      es: 'Líbano',
      en: 'Lebanon',
      de: 'Libanon'
    },
    LC: {
      fr: 'Sainte-Lucie',
      es: 'Santa Lucía',
      en: 'Saint Lucia',
      de: 'St. Lucia'
    },
    LI: {
      fr: 'Liechtenstein',
      es: 'Liechtenstein',
      en: 'Liechtenstein',
      de: 'Liechtenstein'
    },
    LK: {
      fr: 'Sri Lanka',
      es: 'Sri Lanka',
      en: 'Sri Lanka',
      de: 'Sri Lanka'
    },
    LR: {
      fr: 'Liberia',
      es: 'Liberia',
      en: 'Liberia',
      de: 'Liberia'
    },
    LS: {
      fr: 'Lesotho',
      es: 'Lesoto',
      en: 'Lesotho',
      de: 'Lesotho'
    },
    LT: {
      fr: 'Lituanie',
      es: 'Lituania',
      en: 'Lithuania',
      de: 'Litauen'
    },
    LU: {
      fr: 'Luxembourg',
      es: 'Luxemburgo',
      en: 'Luxembourg',
      de: 'Luxemburg'
    },
    LV: {
      fr: 'Lettonie',
      es: 'Letonia',
      en: 'Latvia',
      de: 'Lettland'
    },
    LY: {
      fr: 'Libyenne, Jamahiriya Arabe',
      es: 'Libia',
      en: 'Libya',
      de: 'Libyen'
    },
    MA: {
      fr: 'Maroc',
      es: 'Marruecos',
      en: 'Morocco',
      de: 'Marokko'
    },
    MC: {
      fr: 'Monaco',
      es: 'Mónaco',
      en: 'Monaco',
      de: 'Monaco'
    },
    MD: {
      fr: 'Moldova, Republique de',
      es: 'Moldavia (la República de)',
      en: 'Moldova, Republic of',
      de: 'Moldawien (Republik Moldau)'
    },
    ME: {
      fr: 'Montenegro',
      es: 'Montenegro',
      en: 'Montenegro',
      de: 'Montenegro'
    },
    MF: {
      fr: 'Saint-Martin (partie Francaise)',
      es: 'San Martín (parte francesa)',
      en: 'Saint Martin (French part)',
      de: 'Saint-Martin (franz. Teil)'
    },
    MG: {
      fr: 'Madagascar',
      es: 'Madagascar',
      en: 'Madagascar',
      de: 'Madagaskar'
    },
    MH: {
      fr: 'Marshall, Iles',
      es: 'Islas Marshall (las)',
      en: 'Marshall Islands',
      de: 'Marshallinseln'
    },
    MK: {
      fr: 'Macedoine, l\'ex-Republique Yougoslave de',
      es: 'Macedonia (la antigua República Yugoslava de)',
      en: 'Macedonia, the former Yugoslav Republic of',
      de: 'Mazedonien'
    },
    ML: {
      fr: 'Mali',
      es: 'Malí',
      en: 'Mali',
      de: 'Mali'
    },
    MM: {
      fr: 'Myanmar',
      es: 'Myanmar',
      en: 'Myanmar',
      de: 'Myanmar (Burma)'
    },
    MN: {
      fr: 'Mongolie',
      es: 'Mongolia',
      en: 'Mongolia',
      de: 'Mongolei'
    },
    MO: {
      fr: 'Macao',
      es: 'Macao',
      en: 'Macao',
      de: 'Macao'
    },
    MP: {
      fr: 'Mariannes du Nord, Iles',
      es: 'Islas Marianas del Norte (las)',
      en: 'Northern Mariana Islands',
      de: 'Nördliche Marianen'
    },
    MQ: {
      fr: 'Martinique',
      es: 'Martinica',
      en: 'Martinique',
      de: 'Martinique'
    },
    MR: {
      fr: 'Mauritanie',
      es: 'Mauritania',
      en: 'Mauritania',
      de: 'Mauretanien'
    },
    MS: {
      fr: 'Montserrat',
      es: 'Montserrat',
      en: 'Montserrat',
      de: 'Montserrat'
    },
    MT: {
      fr: 'Malte',
      es: 'Malta',
      en: 'Malta',
      de: 'Malta'
    },
    MU: {
      fr: 'Maurice',
      es: 'Mauricio',
      en: 'Mauritius',
      de: 'Mauritius'
    },
    MV: {
      fr: 'Maldives',
      es: 'Maldivas',
      en: 'Maldives',
      de: 'Malediven'
    },
    MW: {
      fr: 'Malawi',
      es: 'Malaui',
      en: 'Malawi',
      de: 'Malawi'
    },
    MX: {
      fr: 'Mexique',
      es: 'México',
      en: 'Mexico',
      de: 'Mexiko'
    },
    MY: {
      fr: 'Malaisie',
      es: 'Malasia',
      en: 'Malaysia',
      de: 'Malaysia'
    },
    MZ: {
      fr: 'Mozambique',
      es: 'Mozambique',
      en: 'Mozambique',
      de: 'Mosambik'
    },
    NA: {
      fr: 'Namibie',
      es: 'Namibia',
      en: 'Namibia',
      de: 'Namibia'
    },
    NC: {
      fr: 'Nouvelle-Caledonie',
      es: 'Nueva Caledonia',
      en: 'New Caledonia',
      de: 'Neukaledonien'
    },
    NE: {
      fr: 'Niger',
      es: 'Níger (el)',
      en: 'Niger',
      de: 'Niger'
    },
    NF: {
      fr: 'Norfolk, Ile',
      es: 'Isla Norfolk',
      en: 'Norfolk Island',
      de: 'Norfolkinsel'
    },
    NG: {
      fr: 'Nigeria',
      es: 'Nigeria',
      en: 'Nigeria',
      de: 'Nigeria'
    },
    NI: {
      fr: 'Nicaragua',
      es: 'Nicaragua',
      en: 'Nicaragua',
      de: 'Nicaragua'
    },
    NL: {
      fr: 'Pays-bas',
      es: 'Países Bajos (los)',
      en: 'Netherlands',
      de: 'Niederlande'
    },
    NO: {
      fr: 'Norvege',
      es: 'Noruega',
      en: 'Norway',
      de: 'Norwegen'
    },
    NP: {
      fr: 'Nepal',
      es: 'Nepal',
      en: 'Nepal',
      de: 'Nepal'
    },
    NR: {
      fr: 'Nauru',
      es: 'Nauru',
      en: 'Nauru',
      de: 'Nauru'
    },
    NU: {
      fr: 'Niue',
      es: 'Niue',
      en: 'Niue',
      de: 'Niue'
    },
    NZ: {
      fr: 'Nouvelle-Zelande',
      es: 'Nueva Zelanda',
      en: 'New Zealand',
      de: 'Neuseeland'
    },
    OM: {
      fr: 'Oman',
      es: 'Omán',
      en: 'Oman',
      de: 'Oman'
    },
    PA: {
      fr: 'Panama',
      es: 'Panamá',
      en: 'Panama',
      de: 'Panama'
    },
    PE: {
      fr: 'Perou',
      es: 'Perú',
      en: 'Peru',
      de: 'Peru'
    },
    PF: {
      fr: 'Polynesie Francaise',
      es: 'Polinesia Francesa',
      en: 'French Polynesia',
      de: 'Französisch-Polynesien'
    },
    PG: {
      fr: 'Papouasie-Nouvelle-Guinee',
      es: 'Papúa Nueva Guinea',
      en: 'Papua New Guinea',
      de: 'Papua-Neuguinea'
    },
    PH: {
      fr: 'Philippines',
      es: 'Filipinas (las)',
      en: 'Philippines',
      de: 'Philippinen'
    },
    PK: {
      fr: 'Pakistan',
      es: 'Pakistán',
      en: 'Pakistan',
      de: 'Pakistan'
    },
    PL: {
      fr: 'Pologne',
      es: 'Polonia',
      en: 'Poland',
      de: 'Polen'
    },
    PM: {
      fr: 'Saint-Pierre-et-Miquelon',
      es: 'San Pedro y Miquelón',
      en: 'Saint Pierre and Miquelon',
      de: 'Saint-Pierre und Miquelon'
    },
    PN: {
      fr: 'Pitcairn',
      es: 'Pitcairn',
      en: 'Pitcairn',
      de: 'Pitcairninseln'
    },
    PR: {
      fr: 'Porto Rico',
      es: 'Puerto Rico',
      en: 'Puerto Rico',
      de: 'Puerto Rico'
    },
    PS: {
      fr: 'Palestinien Occupe, Territoire',
      es: 'Palestina, Estado de',
      en: 'Palestinian Territory, Occupied',
      de: 'Staat Palästina'
    },
    PT: {
      fr: 'Portugal',
      es: 'Portugal',
      en: 'Portugal',
      de: 'Portugal'
    },
    PW: {
      fr: 'Palaos',
      es: 'Palaos',
      en: 'Palau',
      de: 'Palau'
    },
    PY: {
      fr: 'Paraguay',
      es: 'Paraguay',
      en: 'Paraguay',
      de: 'Paraguay'
    },
    QA: {
      fr: 'Qatar',
      es: 'Catar',
      en: 'Qatar',
      de: 'Katar'
    },
    RE: {
      fr: 'Reunion',
      es: 'Reunión',
      en: 'Réunion',
      de: 'Réunion'
    },
    RO: {
      fr: 'Roumanie',
      es: 'Rumania',
      en: 'Romania',
      de: 'Rumänien'
    },
    RS: {
      fr: 'Serbie',
      es: 'Serbia',
      en: 'Serbia',
      de: 'Serbien'
    },
    RU: {
      fr: 'Russie, Federation de',
      es: 'Rusia, (la) Federación de',
      en: 'Russian Federation',
      de: 'Russische Föderation'
    },
    RW: {
      fr: 'Rwanda',
      es: 'Ruanda',
      en: 'Rwanda',
      de: 'Ruanda'
    },
    SA: {
      fr: 'Arabie Saoudite',
      es: 'Arabia Saudita',
      en: 'Saudi Arabia',
      de: 'Saudi-Arabien'
    },
    SB: {
      fr: 'Salomon, Iles',
      es: 'Islas Salomón (las)',
      en: 'Solomon Islands',
      de: 'Salomonen'
    },
    SC: {
      fr: 'Seychelles',
      es: 'Seychelles',
      en: 'Seychelles',
      de: 'Seychellen'
    },
    SD: {
      fr: 'Soudan',
      es: 'Sudán (el)',
      en: 'Sudan',
      de: 'Sudan'
    },
    SE: {
      fr: 'Suede',
      es: 'Suecia',
      en: 'Sweden',
      de: 'Schweden'
    },
    SG: {
      fr: 'Singapour',
      es: 'Singapur',
      en: 'Singapore',
      de: 'Singapur'
    },
    SH: {
      fr: 'Sainte-Helene, Ascension et Tristan da Cunha',
      es: 'Santa Helena, Ascensión y Tristán de Acuña',
      en: 'Saint Helena, Ascension and Tristan da Cunha',
      de: 'St. Helena'
    },
    SI: {
      fr: 'Slovenie',
      es: 'Eslovenia',
      en: 'Slovenia',
      de: 'Slowenien'
    },
    SJ: {
      fr: 'Svalbard et Ile Jan Mayen',
      es: 'Svalbard y Jan Mayen',
      en: 'Svalbard and Jan Mayen',
      de: 'Svalbard und Jan Mayen'
    },
    SK: {
      fr: 'Slovaquie',
      es: 'Eslovaquia',
      en: 'Slovakia',
      de: 'Slowakei'
    },
    SL: {
      fr: 'Sierra Leone',
      es: 'Sierra leona',
      en: 'Sierra Leone',
      de: 'Sierra Leone'
    },
    SM: {
      fr: 'Saint-Marin',
      es: 'San Marino',
      en: 'San Marino',
      de: 'San Marino'
    },
    SN: {
      fr: 'Senegal',
      es: 'Senegal',
      en: 'Senegal',
      de: 'Senegal'
    },
    SO: {
      fr: 'Somalie',
      es: 'Somalia',
      en: 'Somalia',
      de: 'Somalia'
    },
    SR: {
      fr: 'Suriname',
      es: 'Surinam',
      en: 'Suriname',
      de: 'Suriname'
    },
    SS: {
      fr: 'Soudan du Sud',
      es: 'Sudán del Sur',
      en: 'South Sudan',
      de: 'Südsudan'
    },
    ST: {
      fr: 'Sao Tome-et-Principe',
      es: 'Santo Tomé y Príncipe',
      en: 'Sao Tome and Principe',
      de: 'São Tomé und Príncipe'
    },
    SV: {
      fr: 'El Salvador',
      es: 'El Salvador',
      en: 'El Salvador',
      de: 'El Salvador'
    },
    SX: {
      fr: 'Saint-Martin (partie Neerlandaise)',
      es: 'Sint Maarten (parte holandesa)',
      en: 'Sint Maarten (Dutch part)',
      de: 'Sint Maarten (niederl. Teil)'
    },
    SY: {
      fr: 'Syrienne, Republique Arabe',
      es: 'Siria, (la) República Árabe',
      en: 'Syrian Arab Republic',
      de: 'Syrien, Arabische Republik'
    },
    SZ: {
      fr: 'Swaziland',
      es: 'Suazilandia',
      en: 'Swaziland',
      de: 'Swasiland'
    },
    TC: {
      fr: 'Turks et Caiques, Iles',
      es: 'Islas Turcas y Caicos (las)',
      en: 'Turks and Caicos Islands',
      de: 'Turks- und Caicosinseln'
    },
    TD: {
      fr: 'Tchad',
      es: 'Chad',
      en: 'Chad',
      de: 'Tschad'
    },
    TF: {
      fr: 'Terres Australes Francaises',
      es: 'Territorios Australes Franceses (los)',
      en: 'French Southern Territories',
      de: 'Französische Süd- und Antarktisgebiete'
    },
    TG: {
      fr: 'Togo',
      es: 'Togo',
      en: 'Togo',
      de: 'Togo'
    },
    TH: {
      fr: 'Thailande',
      es: 'Tailandia',
      en: 'Thailand',
      de: 'Thailand'
    },
    TJ: {
      fr: 'Tadjikistan',
      es: 'Tayikistán',
      en: 'Tajikistan',
      de: 'Tadschikistan'
    },
    TK: {
      fr: 'Tokelau',
      es: 'Tokelau',
      en: 'Tokelau',
      de: 'Tokelau'
    },
    TL: {
      fr: 'Timor-leste',
      es: 'Timor-Leste',
      en: 'Timor-Leste',
      de: 'Osttimor (Timor-Leste)'
    },
    TM: {
      fr: 'Turkmenistan',
      es: 'Turkmenistán',
      en: 'Turkmenistan',
      de: 'Turkmenistan'
    },
    TN: {
      fr: 'Tunisie',
      es: 'Túnez',
      en: 'Tunisia',
      de: 'Tunesien'
    },
    TO: {
      fr: 'Tonga',
      es: 'Tonga',
      en: 'Tonga',
      de: 'Tonga'
    },
    TR: {
      fr: 'Turquie',
      es: 'Turquía',
      en: 'Turkey',
      de: 'Türkei'
    },
    TT: {
      fr: 'Trinite-et-Tobago',
      es: 'Trinidad y Tobago',
      en: 'Trinidad and Tobago',
      de: 'Trinidad und Tobago'
    },
    TV: {
      fr: 'Tuvalu',
      es: 'Tuvalu',
      en: 'Tuvalu',
      de: 'Tuvalu'
    },
    TW: {
      fr: 'Taiwan, Province de Chine',
      es: 'Taiwán (Provincia de China)',
      en: 'Taiwan, Province of China',
      de: 'Republik China (Taiwan)'
    },
    TZ: {
      fr: 'Tanzanie, Republique-Unie de',
      es: 'Tanzania, República Unida de',
      en: 'Tanzania, United Republic of',
      de: 'Tansania, Vereinigte Republik'
    },
    UA: {
      fr: 'Ukraine',
      es: 'Ucrania',
      en: 'Ukraine',
      de: 'Ukraine'
    },
    UG: {
      fr: 'Ouganda',
      es: 'Uganda',
      en: 'Uganda',
      de: 'Uganda'
    },
    UM: {
      fr: 'Iles Mineures Eloignees des Etats-Unis',
      es: 'Islas de Ultramar Menores de Estados Unidos (las)',
      en: 'United States Minor Outlying Islands',
      de: 'United States Minor Outlying Islands'
    },
    US: {
      fr: 'Etats-Unis',
      es: 'Estados Unidos (los)',
      en: 'United States',
      de: 'Vereinigte Staaten von Amerika'
    },
    UY: {
      fr: 'Uruguay',
      es: 'Uruguay',
      en: 'Uruguay',
      de: 'Uruguay'
    },
    UZ: {
      fr: 'Ouzbekistan',
      es: 'Uzbekistán',
      en: 'Uzbekistan',
      de: 'Usbekistan'
    },
    VA: {
      fr: 'Saint-siege (etat de la cite du Vatican)',
      es: 'Santa Sede',
      en: 'Holy See (Vatican City State)',
      de: 'Vatikanstadt'
    },
    VC: {
      fr: 'Saint-Vincent-et-les Grenadines',
      es: 'San Vicente y las Granadinas',
      en: 'Saint Vincent and the Grenadines',
      de: 'St. Vincent und die Grenadinen'
    },
    VE: {
      fr: 'Venezuela, Republique Bolivarienne du',
      es: 'Venezuela, República Bolivariana de',
      en: 'Venezuela, Bolivarian Republic of',
      de: 'Venezuela'
    },
    VG: {
      fr: 'Iles Vierges Britanniques',
      es: 'Islas Vírgenes (Británicas)',
      en: 'Virgin Islands, British',
      de: 'Britische Jungferninseln'
    },
    VI: {
      fr: 'Iles Vierges des Etats-Unis',
      es: 'Islas Vírgenes (EE.UU.)',
      en: 'Virgin Islands, U.S.',
      de: 'Amerikanische Jungferninseln'
    },
    VN: {
      fr: 'Viet Nam',
      es: 'Viet Nam',
      en: 'Viet Nam',
      de: 'Vietnam'
    },
    VU: {
      fr: 'Vanuatu',
      es: 'Vanuatu',
      en: 'Vanuatu',
      de: 'Vanuatu'
    },
    WF: {
      fr: 'Wallis et Futuna',
      es: 'Wallis y Futuna',
      en: 'Wallis and Futuna',
      de: 'Wallis und Futuna'
    },
    WS: {
      fr: 'Samoa',
      es: 'Samoa',
      en: 'Samoa',
      de: 'Samoa'
    },
    YE: {
      fr: 'Yemen',
      es: 'Yemen',
      en: 'Yemen',
      de: 'Jemen'
    },
    YT: {
      fr: 'Mayotte',
      es: 'Mayotte',
      en: 'Mayotte',
      de: 'Mayotte'
    },
    ZA: {
      fr: 'Afrique du Sud',
      es: 'Sudáfrica',
      en: 'South Africa',
      de: 'Südafrika'
    },
    ZM: {
      fr: 'Zambie',
      es: 'Zambia',
      en: 'Zambia',
      de: 'Sambia'
    },
    ZW: {
      fr: 'Zimbabwe',
      es: 'Zimbabue',
      en: 'Zimbabwe',
      de: 'Simbabwe'
    }
  };

  var supportedLanguages = ['en', 'es', 'fr', 'de'];

  function getNameByCode(code, language) {
    language = language || 'en';
    if (supportedLanguages.indexOf(language) < 0) {
        language = 'en';
    }

    var result = 'unknown';
    if (Object.keys(countryData).indexOf(code) !== -1) {
      result = countryData[code][language];
    }
    return result;
  };

  function getCodeByName(name, language) {
    language = language || 'en';

    if (supportedLanguages.indexOf(language) < 0) {
        language = 'en';
    }

    //var countryData = countryData;
    var foundCountryCode;

    Object.keys(countryData).forEach(function(countryCode) {
      var countryName = countryData[countryCode][language];

      if (name === countryName) {
        foundCountryCode = countryCode;
      }

    });

    return foundCountryCode;
  };

  function getCodes(language) {
    language = language || 'en';
    if (supportedLanguages.indexOf(language) < 0) {
        language = 'en';
    }

    return Object.keys(countryData);
  };

  function getNames(language) {
    language = language || 'en';

    if (supportedLanguages.indexOf(language) < 0) {
        language = 'en';
    }

    var names = [];

    Object.keys(countryData).forEach(function(countryCode) {
      if (countryData[countryCode][language]) {
        names.push(countryData[countryCode][language]);
      }
    });

    names = names.sort(function(a,b) {
      return a.localeCompare(b);
    });

    return names;
  };

  return {
    getNames     : getNames,
    getCodes     : getCodes,
    getNameByCode: getNameByCode,
    getCodeByName: getCodeByName
  };
}));
