(function() {
  'use strict';

  angular.module('homerez.countries', ['pascalprecht.translate'])
    .filter('countryName',        countryName)
    .filter('orderByCountryName', orderByCountryName);

  orderByCountryName.$inject = ['$translate', '$filter'];
  countryName.$inject        = ['$translate'];


  function orderByCountryName($translate, $filter) {
    return function(array, objKey) {
      var result = [];
      var translated = [];
      angular.forEach(array, function(value) {
        var translateKey = objKey ? value[objKey] : value;
        var currentLanguage = $translate.use() || 'en';
        translated.push({
          key: value,
          label: hipCountries.getNameByCode(translateKey, currentLanguage.substr(0,2))
        });
      });
      angular.forEach($filter('orderBy')(translated, 'label'), function(sortedObject) {
        result.push(sortedObject.key);
      });
      return result;
    };
  }

  function countryName($translate) {
    return function(countryCode) {
      var currentLanguage = $translate.use() || 'en';
      return hipCountries.getNameByCode(countryCode, currentLanguage.substr(0,2));
    };
  }

}());
