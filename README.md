# hipCountries 0.3.0

Utility functions for countries supported in HIP

## getNames()
Get all English country names

## getCodes()
get all two-letter country codes

## getNameByCode(code)
Get an English country name by its two-letter code

## getCodeByName(code)
Get an English country name by its two-letter code
